# Sparse CP #

This is a trivial tool to copy a file over another, touching only the differences.

The main use of this is migrating disk images --- with snapshots --- into ZFS volumes, while minimizing the space needed.

# Usage #

First create the ZFS volume `tank/dest_vol` and copy the oldest snapshot to it --- using `dd`, for example.
Then, for each subsequnt snapshot and the final version:

1. Create a ZFS snapshot: `zfs snapshot tank/dest_vol@my_snap`
2. Copy the newer version onto the ZFS volume: `cargo run --release -- source_disk.img /dev/tank/dest_vol`
