use std::fs::{File, OpenOptions};
use std::io::{Read, Seek, SeekFrom, Write};

use anyhow::Result;

use clap::{App, Arg};

fn main() {
    let matches = App::new("My Super Program")
        .version("1.0")
        .author("Kevin K. <kbknapp@gmail.com>")
        .about("Does awesome things")
        .arg(
            Arg::with_name("INPUT")
                .help("Sets the input file to use")
                .required(true)
                .index(1),
        )
        .arg(
            Arg::with_name("OUTPUT")
                .help("Sets the output file to use")
                .required(true)
                .index(2),
        )
        .arg(
            Arg::with_name("v")
                .short("v")
                .multiple(true)
                .help("Sets the level of verbosity"),
        )
        .get_matches();

    let input_path = matches.value_of("INPUT").unwrap();
    println!("Using input file: {}", input_path);
    let output_path = matches.value_of("OUTPUT").unwrap();
    println!("Using input file: {}", output_path);

    let mut input_file = File::open(input_path).unwrap();
    let mut output_file = OpenOptions::new()
        .read(true)
        .write(true)
        .create(true)
        .open(output_path)
        .unwrap();

    let (n, m) = copy_sparse(input_file, output_file, 8192).unwrap();
    println!("Read {} bytes\nWritten {} bytes", n, m);

    // match matches.occurrences_of("v") {
    //     0 => println!("No verbose info"),
    //     1 => println!("Some verbose info"),
    //     2 => println!("Tons of verbose info"),
    //     3 | _ => println!("Don't be crazy"),
    // }
}

fn copy_sparse<I: Read, O: Read + Write + Seek>(
    mut input: I,
    mut output: O,
    blocksize: usize,
) -> Result<(usize, usize)> {
    assert!(blocksize <= 2usize.pow(63) - 1);
    let mut buf_in: Vec<u8> = vec![0; blocksize];
    let mut buf_out: Vec<u8> = vec![0; blocksize];
    let mut tot_read: usize = 0;
    let mut tot_write: usize = 0;
    loop {
        let n = input.read(&mut buf_in)?;
        tot_read += n;
        if n == 0 {
            return Ok((tot_read, tot_write));
        }

        let m = output.read(&mut buf_out[..n])?;

        if m != n || buf_out[..n].iter().ne(buf_in[..n].iter()) {
            output.seek(SeekFrom::Current(-(m as i64)))?;
            let m = output.write(&buf_in[..n])?;
            tot_write += m;
            assert_eq!(m, n);
        }
    }
}
